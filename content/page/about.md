---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Here's a bit about me:

- A founder and director of https://lab577.io/
- CTO / Technical Lead for [DASL](https://lab577.io/dasl/) a digital asset platform
- Lover of and contributor to the [Corda Project](https://www.corda.net/)
- Massive fan of decentralised ledgers and blockchains in general. I started hacking on them back in 2012 and [formally appeared](https://www.finextra.com/newsarticle/26704/rbs-embraces-crypto-currencies-in-hackathon-challenge) in the scene back in 2014
- Used to work in various industries including financial services, work on video and 3D avatar systems in MPEG-7, 3D engines, motion tracking, a broad range of distributed systems, natural language processing, AI, with some [published work](https://www.researchgate.net/profile/Farzad_Pezeshkpour). You can find out more about me over at [LinkedIn](https://www.linkedin.com/in/farzadpezeshkpour/)

That rug really tied the room together.
