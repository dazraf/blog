---
title: "Advent of Code"
subtitle: "For coding and hiring"
date: 2019-12-28
tags: ["coding","hiring"]
---

This Christmas, in the middle of all the festive fun and chaos, I tried out a few challenges in the awesome [Advent of Code](https://adventofcode.com/). Apart from being a very addictive and fun way of improving your coding skills in a familiar or new language, it's also a great way of looking for potential new hires! The challenges range from easy to advanced, exercising various skills, be it design, testing, data structures, algorithms, virtual machines, or documentation. I have a private repo for my solutions - will open it up in the new year, once I've completed a few more challenges. In the meantime, check out the [leaderboard](https://adventofcode.com/2019/leaderboard)!