---
title: "Great iOS Git Client and Editor"
subtitle: "Git everywhere..."
date: 2020-01-26
tags: ["tools","mobile","git"]
---

Does this happen to you? There's often times that I am away from my desk without a laptop (or network!) when an idea pops into my head and I want to either document it, write some code, or blog about it on my Gitlab served blog. 

Gitlab and Github provide modest browser-based tools but these all are rather clunky or fiddly on a mobile device. 

The great news is that I've now found the perfect solution on iOS 🙂 It's called [Working Copy](https://apps.apple.com/gb/app/working-copy-git-client/id896694807). A fully featured Git client with great editing and preview facilities, including a javascript engine for running your JS files, a great Markdown editor, diff and merge, and history tools - it's awesome!

> This post was written using Working Copy 😎
