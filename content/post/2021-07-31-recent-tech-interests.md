---
title: "Recent tech interests"
subtitle: "Always fun to keep learning"
date: 2021-07-31
tags: ["tech"]
---

* [libp2p](https://libp2p.io/)
* [xinfin](https://xinfin.org/)
* [polkadot](https://polkadot.network/)
* [graphql](https://graphql.org/)
